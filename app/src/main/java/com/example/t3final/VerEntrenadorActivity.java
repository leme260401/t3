package com.example.t3final;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.t3final.R;
import com.example.t3final.pokemons.Entrenador;
import com.example.t3final.pokemons.Pokemons;
import com.example.t3final.recycler.AdapterPokemon;
import com.example.t3final.services.Servicio;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VerEntrenadorActivity extends AppCompatActivity {

    private ImageView ivImagen;
    private TextView tvNombre;
    private TextView tvTipo;
    private Button btnUbicacion;
    private String urlImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_entrenador);

        tvNombre = findViewById(R.id.tvNombre);
        tvTipo = findViewById(R.id.tvTipo);
        ivImagen = findViewById(R.id.ivImage);
        btnUbicacion = findViewById(R.id.btnUicacion);

        btnUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerEntrenadorActivity.this, MainActivity.class);

                startActivity(intent);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Servicio service = retrofit.create(Servicio.class);

        service.getEntrenador().enqueue(new Callback<Entrenador>() {
            @Override
            public void onResponse(Call<Entrenador> call, Response<Entrenador> response) {
                if (response.code()==200){
                    Entrenador entrenador = response.body();
                    Log.i("MY_APP" , "Emtrenador es: "+entrenador);

                    urlImage = entrenador.getImagen();
                    tvNombre.setText(entrenador.getNombres());
                    tvTipo.setText(entrenador.getPueblo());

                    Glide.with(getApplicationContext()).load(urlImage).into(ivImagen);
                }else {
                    Log.i("MY_APP" , "Intentalo de nuevo, algo falló");
                }
            }

            @Override
            public void onFailure(Call<Entrenador> call, Throwable t) {
                Log.i("MY_APP" , "No pudimos conectarnos con la aplicación");
            }
        });
    }
}