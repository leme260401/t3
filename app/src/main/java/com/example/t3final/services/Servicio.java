package com.example.t3final.services;

import com.example.t3final.pokemons.Entrenador;
import com.example.t3final.pokemons.Pokemons;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Servicio {
    @GET("pokemons/N00170218")
    Call<List<Pokemons>> getALL();

    @POST("pokemons/N00170218/crear")
    Call<Pokemons> create(@Body Pokemons pokemon);

    @GET("entrenador/N00170218")
    Call<Entrenador> getEntrenador();

    @POST("entrenador/N00170218")
    Call<Entrenador> createEntrenador(@Body Entrenador entrenador);
}
