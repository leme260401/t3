package com.example.t3final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.t3final.pokemons.Pokemons;
import com.google.gson.Gson;

public class DetalleActivity extends AppCompatActivity {

    private ImageView ivImagen;
    private TextView tvNombre;
    private TextView tvTipo;
    private Button btnUbicacion;
    private String urlImage;
    private String latitude;
    private String longitude;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        tvNombre = findViewById(R.id.tvNombre);
        tvTipo = findViewById(R.id.tvTipo);
        ivImagen = findViewById(R.id.ivImage);
        btnUbicacion = findViewById(R.id.btnUicacion);
        path = "https://upn.lumenes.tk";

        Intent intent = getIntent();
        String pokemon = intent.getStringExtra("Pokemon");
        Pokemons pkm = new Gson().fromJson(pokemon, Pokemons.class);

        urlImage = path + pkm.getUrl_imagen();
        tvNombre.setText(pkm.getNombre());
        tvTipo.setText(pkm.getTipo());
        tvNombre.setText(pkm.getNombre());

        latitude = String.valueOf(pkm.getLatitude());
        longitude = String.valueOf(pkm.getLongitude());

        Glide.with(getApplicationContext()).load(urlImage).into(ivImagen);

        btnUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetalleActivity.this, MapsActivity.class);
                intent.putExtra("Latitude", latitude);
                intent.putExtra("Longitude", longitude);
                startActivity(intent);
            }
        });
    }
}